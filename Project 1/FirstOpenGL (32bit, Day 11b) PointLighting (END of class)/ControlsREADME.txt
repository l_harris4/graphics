Controls///////

I to toggle between controlling the camera/object/light


//////if controlling camera//////
W and S to move the camera up and down
Q and E to change the direction the camera is facing upward or downward
A and D to change the direction the camera is facing left or right
up arrow to move the camera forward
down arrow to move the camera backward

//////if controlling object/light//////
W and S for z axis
A and D for x axis
Q and E for y axis
U to move a different object/light

///////just object controls//////
O to change an object between wireframe or not
1-6 to control different rotations

//////just light controls///////
enter key to turn off/on a few lights

Object Start
type: plane
normals: true
physics: false
wireframe: true
filename: Platform.ply
scale: 1
colour: 0.7,0.4,0.3
Object End

Object Start
type: plane
normals: true
physics: false
wireframe: true
filename: LeftWall.ply
scale: 1
colour: 0.7,0.4,0.3
Object End

//main sphere

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -4,4,0
velocity: 2,1,0
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.7,0.4,0.3
Object End

//dynamic environment
Object Start
type: cube
normals: true
physics: true
wireframe: true
gravity: false
editable: true
velocity: -3,0,0
filename: CubePlatform.ply
scale: 1
colour: 1,0,0
Object End

Object Start
type: cube
normals: true
physics: true
wireframe: true
gravity: false
editable: true
position: -50,0,-10
velocity: 3,0,0
filename: CubePlatform.ply
scale: 1
colour: 0,1,0
Object End

Object Start
type: cube
normals: true
physics: true
wireframe: true
gravity: false
editable: true
position: -20,0,-30
velocity: 3,0,0
filename: CubePlatform.ply
scale: 1
colour: 0,0,1
Object End

//more spheres
type: sphere
Object Start
normals: true
physics: true
wireframe: false
gravity: true
position: -20.5,4,0
velocity: 0,0,0
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -0.5,4.0,-24.0
velocity: -3.3,0,0
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -10.5,4,-24
velocity: 0,0,3.3
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -10.5,3,-28
velocity: 0,0,3.3
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -10.5,3,-34
velocity: 3.3,0,0
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: 10.5,3,-34
velocity: 3.3,0,0
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -20.5,3,-34
velocity: 0,0,-3.3
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -10.5,3,-54
velocity: 3.3,0,0
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

Object Start
type: sphere
normals: true
physics: true
wireframe: false
gravity: true
position: -20.5,3,-54
velocity: 0,0,3.3
filename: Sphereply_xyz_n.ply
scale: 1
colour: 0.0,0.0,1
Object End

