#include "cGameObject.h"

cGameObject::cGameObject()
{
	this->scale = 1.0f;
	this->position = glm::vec3(0.0f);
	this->orientation = glm::vec3(0.0f);
	this->orientation2 = glm::vec3(0.0f);

	this->vel = glm::vec3(0.0f);
	this->accel = glm::vec3(0.0f);	

	this->diffuseColour = glm::vec4( 0.0f, 0.0f, 0.0f, 1.0f );

	//Assume everything is simulated 
	this->bIsUpdatedInPhysics = true; 
	this->radius = 0.0f;

	this->typeOfObject = eTypeOfObject::UNKNOWN;
	this->bIsWireframe = false;

	return;
}

cGameObject::~cGameObject()
{
	return;
}