--------------------------------------------
CONTROLS
--------------------------------------------
A/D to turn the camera left or right
W/S to turn the camera up or down
Q/E to move the camera forward and backward
O to switch into bounding box mode
Z to switch into colour mode
SPACE to switch into only texture mode