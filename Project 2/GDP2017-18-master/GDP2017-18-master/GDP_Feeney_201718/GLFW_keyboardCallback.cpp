#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"

#include <iostream>

bool isShiftKeyDown( int mods, bool bByItself = true );
bool isCtrlKeyDown( int mods, bool bByItself = true );
bool isAltKeyDown( int mods, bool bByItself = true );

extern int g_selectedGameObjectIndex;
extern int g_selectedLightIndex;
extern bool g_movingGameObject;
extern bool g_lightsOn;
extern bool g_texturesOn;
extern bool g_movingLights;
extern bool g_boundingBoxes;
extern cCamera* g_pTheCamera;

const float MOVESPEED = 0.3f;
const float CAMERAROTATIONSPEED = 1;
const float CAMERASPEED = 2;
//extern cGameObject* cameraTargetObject;

void updateCamera(glm::vec3 turnChange)
{
	g_pTheCamera->theObject->adjustQOrientationFormDeltaEuler(turnChange);
	glm::vec3 targetPosition = g_pTheCamera->theObject->position;

	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->theObject->qOrientation) *
		glm::vec4(0, 1, 60, 1);

	g_pTheCamera->target = targetPosition;

	glm::vec3 position = glm::vec3(tempPos.x + g_pTheCamera->theObject->position.x
		, tempPos.y + g_pTheCamera->theObject->position.y,
		tempPos.z + g_pTheCamera->theObject->position.z);

	g_pTheCamera->eye = position;
}

void updateCamera()
{
	glm::vec3 targetPosition = g_pTheCamera->theObject->position;

	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->theObject->qOrientation) *
		glm::vec4(0, 1, 60, 1);

	g_pTheCamera->target = targetPosition;

	glm::vec3 position = glm::vec3(tempPos.x + g_pTheCamera->theObject->position.x
		, tempPos.y + g_pTheCamera->theObject->position.y,
		tempPos.z + g_pTheCamera->theObject->position.z);

	g_pTheCamera->eye = position;
}


/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

	cGameObject* pLeftTeapot = findObjectByFriendlyName(LEFTTEAPOTNAME, ::g_vecGameObjects);

	switch (key)
	{
	case GLFW_KEY_SPACE:
		if (action == GLFW_PRESS)
		{
			g_lightsOn = !g_lightsOn;
		}
		break;
	case GLFW_KEY_Z:
		if (action == GLFW_PRESS)
		{
			g_texturesOn = !g_texturesOn;
		}
		break;
	case GLFW_KEY_U://cycle through objects
		if (action == GLFW_PRESS)
		{
			::g_selectedGameObjectIndex++;
			if (::g_selectedGameObjectIndex >= ::g_vecGameObjects.size())
				::g_selectedGameObjectIndex = 0;
			::g_selectedLightIndex++;
			if (::g_selectedLightIndex >= ::g_pLightManager->vecLights.size())
				::g_selectedLightIndex = 0;
		}
		break;
	case GLFW_KEY_I://Toggle between controlling light/object/camera
		if (action == GLFW_PRESS)
		{
			if (::g_movingGameObject)
			{
				::g_movingGameObject = false;
				::g_movingLights = true;
			}
			else if (::g_movingLights)
			{
				::g_movingLights = false;
			}
			else
			{
				::g_movingGameObject = true;
			}
		}
		break;
	case GLFW_KEY_A:// Move an object or light negative in x axis, or rotate camera negative in x axis
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x -= MOVESPEED;
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].position.x -= MOVESPEED;
			if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
			{
				int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;
				::g_vecGameObjects[localIndex]->position.x -= MOVESPEED;
			}
		}
		else
		{
			updateCamera(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
		}
		break;
	case GLFW_KEY_D:// Move an object or light positive in x axis, or rotate camera positive in x axis
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x += MOVESPEED;
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].position.x += MOVESPEED;
			if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
			{
				int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;
				::g_vecGameObjects[localIndex]->position.x += MOVESPEED;
			}
		}
		else
		{
			updateCamera(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
		}
		break;
	case GLFW_KEY_W:// Move an object or light negative in z axis, or rotate camera negative in z axis
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z -= MOVESPEED;
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].position.z -= MOVESPEED;
			if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
			{
				int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;
				::g_vecGameObjects[localIndex]->position.z -= MOVESPEED;
			}
		}
		else
		{
			updateCamera(glm::vec3(glm::radians(-CAMERAROTATIONSPEED), 0, 0));
		}
		break;
	case GLFW_KEY_S:// Move an object or light positive in z axis, or rotate camera positive in z axis
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z += MOVESPEED;
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].position.z += MOVESPEED;
			if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
			{
				int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;
				::g_vecGameObjects[localIndex]->position.z += MOVESPEED;
			}
		}
		else
		{
			updateCamera(glm::vec3(glm::radians(CAMERAROTATIONSPEED), 0, 0));
		}
		break;
	case GLFW_KEY_Q:// Move an object or light negative in y axis, or rotate camera negative in y axis
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y -= MOVESPEED;
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].position.y -= MOVESPEED;
			if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
			{
				int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;
				::g_vecGameObjects[localIndex]->position.y -= MOVESPEED;
			}
		}
		else
		{
			glm::vec4 tempPos = glm::toMat4(g_pTheCamera->theObject->qOrientation) *
				glm::vec4(0, 0, CAMERASPEED, 1);

			glm::vec3 position = glm::vec3(tempPos.x + g_pTheCamera->theObject->position.x
				, tempPos.y + g_pTheCamera->theObject->position.y,
				tempPos.z + g_pTheCamera->theObject->position.z);

			g_pTheCamera->theObject->position = position;
			updateCamera();
		}
		break;
	case GLFW_KEY_E:// Move an object or light positive in y axis, or rotate camera positive in y axis
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y += MOVESPEED;
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].position.y += MOVESPEED;
			if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
			{
				int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;
				::g_vecGameObjects[localIndex]->position.y += MOVESPEED;
			}
		}
		else
		{
			glm::vec4 tempPos = glm::toMat4(g_pTheCamera->theObject->qOrientation) *
				glm::vec4(0, 0, -CAMERASPEED, 1);

			glm::vec3 position = glm::vec3(tempPos.x + g_pTheCamera->theObject->position.x
				, tempPos.y + g_pTheCamera->theObject->position.y,
				tempPos.z + g_pTheCamera->theObject->position.z);

			g_pTheCamera->theObject->position = position;
			updateCamera();
		}
		break;
	case GLFW_KEY_P: //used for debugging, printing the current location of object
		if (::g_movingGameObject)
		{
			std::cout << "x:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x
				<< " y:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y
				<< " z:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z
				<< std::endl;

			/*std::cout << "ox:" << glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.x)
				<< " oy:" << glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.y)
				<< " oz:" << glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.z)
				<< std::endl;*/
		}
		else if (::g_movingLights)
		{
			std::cout << "x:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.x
				<< " y:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.y
				<< " z:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.z
				<< std::endl;
		}
		break;

	case GLFW_KEY_O: //Switching an object between wireframe or not
		if (action == GLFW_PRESS) {
			g_boundingBoxes = !g_boundingBoxes;
		}
		break;
	case GLFW_KEY_L: //Switching an object between wireframe or not
		if (action == GLFW_PRESS) {
			//SaveDataToFile();
		}
		break;
	case GLFW_KEY_C: //duplicating an object
		if (action == GLFW_PRESS) {
			cGameObject* pTempGO = new cGameObject();
			pTempGO->bIsUpdatedInPhysics = ::g_vecGameObjects[::g_selectedGameObjectIndex]->bIsUpdatedInPhysics;
			pTempGO->bIsWireFrame = ::g_vecGameObjects[::g_selectedGameObjectIndex]->bIsWireFrame;
			pTempGO->diffuseColour = ::g_vecGameObjects[::g_selectedGameObjectIndex]->diffuseColour;
			pTempGO->meshName = ::g_vecGameObjects[::g_selectedGameObjectIndex]->meshName;
			pTempGO->qOrientation = ::g_vecGameObjects[::g_selectedGameObjectIndex]->qOrientation;
			//pTempGO->orientation = ::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation;
			//pTempGO->orientation2 = ::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2;
			pTempGO->position = ::g_vecGameObjects[::g_selectedGameObjectIndex]->position;
			pTempGO->radius = ::g_vecGameObjects[::g_selectedGameObjectIndex]->radius;
			pTempGO->scale = ::g_vecGameObjects[::g_selectedGameObjectIndex]->scale;
			pTempGO->typeOfObject = ::g_vecGameObjects[::g_selectedGameObjectIndex]->typeOfObject;
			pTempGO->vel = ::g_vecGameObjects[::g_selectedGameObjectIndex]->vel;

			::g_vecGameObjects.push_back(pTempGO);

		}
		break;

		//1 - 6 are used to change the rotation of an object
	case GLFW_KEY_1:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(glm::radians(1.0f),0,0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.x += 0.01;
		}

		break;
	case GLFW_KEY_2:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(glm::radians(-1.0f), 0, 0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.x -= 0.01;
		}
		break;
	case GLFW_KEY_3:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, glm::radians(1.0f), 0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.y += 0.01;
		}
		break;
	case GLFW_KEY_4:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, glm::radians(-1.0f), 0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.y -= 0.01;
		}
		break;
	case GLFW_KEY_5:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, 0, glm::radians(1.0f)));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.z += 0.01;
		}
		break;
	case GLFW_KEY_6:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, 0, glm::radians(-1.0f)));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.z -= 0.01;
		}
		break;

	case GLFW_KEY_UP: //Move the camera "forward"
	{
		//
	}
	break;
	case GLFW_KEY_DOWN: //Move the camera "backward"
	{
	}
	break;


	}
	// HACK: print output to the console
//	std::cout << "Light[0] linear atten: "
//		<< ::g_pLightManager->vecLights[0].attenuation.y << ", "
//		<< ::g_pLightManager->vecLights[0].attenuation.z << std::endl;
	return;
}



// Helper functions
bool isShiftKeyDown( int mods, bool bByItself /*=true*/ )
{
	if ( bByItself )
	{	// shift by itself?
		if ( mods == GLFW_MOD_SHIFT )	{ return true; }
		else							{ return false; }
	}
	else
	{	// shift with anything else, too
		if ( ( mods && GLFW_MOD_SHIFT ) == GLFW_MOD_SHIFT )	{	return true;	}
		else												{	return false;	}
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

//bool isCtrlKeyDown( int mods, bool bByItself = true );
//bool isAltKeyDown( int mods, bool bByItself = true );