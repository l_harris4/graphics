// Include glad and GLFW in correct order
#include "globalOpenGL_GLFW.h"


#include <iostream>			// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr


#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>
#include <time.h>

#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"
#include "cModelAssetLoader.h"
#include <algorithm>
#include "cEmitter.h"
#include "cCamera.h"


#include "Physics.h"	// Physics collision detection functions

#include "cLightManager.h"

// Include all the things that are accessed in other files
#include "globalGameStuff.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

// Forward declaration of the function
void DrawObject(cGameObject* pTheGO);
//void DrawParticle(cParticle* pThePart);
void LightFlicker(double curTime);
void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID);



// Used by the light drawing thingy
// Will draw a wireframe sphere at this location with this colour
//void DrawDebugSphere(glm::vec3 location, glm::vec4 colour, float scale);
//cGameObject* g_pTheDebugSphere;

//	static const int MAXNUMBEROFGAMEOBJECTS = 10;
//	cGameObject* g_GameObjects[MAXNUMBEROFGAMEOBJECTS];

// Remember to #include <vector>...
std::vector< cGameObject* >  g_vecGameObjects;

cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr

//cShaderManager	g_ShaderManager;			// Stack (no new)
cShaderManager*		g_pShaderManager = 0;		// Heap, new (and delete)
cLightManager*		g_pLightManager = 0;
CTextureManager*		g_pTextureManager = 0;

cDebugRenderer*			g_pDebugRenderer = 0;
cCamera* g_pTheCamera = NULL;


// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
							// w = shininess of the 
GLint uniLoc_bIsDebugWireFrameObject = -1;
GLint uniLoc_bUsingLighting = -1;
GLint uniLoc_bUsingTextures= -1;
GLint uniLoc_bDiscardTexture = -1;

GLint uniLoc_eyePosition = -1;	// Camera position
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;

GLint texSampCube00_LocID = -1;
GLint texSampCube01_LocID = -1;
GLint texSampCube02_LocID = -1;
GLint texSampCube03_LocID = -1;

GLint texCubeBlend00_LocID = -1;
GLint texCubeBlend01_LocID = -1;
GLint texCubeBlend02_LocID = -1;
GLint texCubeBlend03_LocID = -1;

int g_selectedGameObjectIndex = 0;
int g_selectedLightIndex = 0;
bool g_movingGameObject = false;
bool g_lightsOn = false;
bool g_texturesOn = false;
bool g_movingLights = false;
bool g_boundingBoxes = false;
const float MOVESPEED = 0.3f;
const float ROTATIONSPEED = -2;
const float CAMERASPEED = 0.2f;

cEmitter* g_pTestParticles;
std::vector< cParticle* > g_vecParticlesToDraw;
//cGameObject* cameraTargetObject = new cGameObject();


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

struct DistanceFunc
{
	DistanceFunc(const glm::vec3& _p) : p(_p) {}

	bool operator()(const cGameObject* lhs, const cGameObject* rhs) const
	{
		return glm::distance(p, lhs->position) > glm::distance(p, rhs->position);
	}

private:
	glm::vec3 p;
};


void sortObjectsBasedOnCamera()
{
	glm::vec3 eye = g_pTheCamera->eye;
	for (int index = 0; index != g_vecGameObjects.size() - 1; index++)
	{
		cGameObject* p1 = g_vecGameObjects[index];
		cGameObject* p2 = g_vecGameObjects[index + 1];

		// Use glm distance 
		// (note, you can use a "squared distance" function
		//  that does not do the square root, as it's faster)
		glm::vec3 p1Pos(p1->position.x, p1->position.y, p1->position.z);
		glm::vec3 p2Pos(p2->position.x, p2->position.y, p2->position.z);


		if (glm::distance(eye, p1Pos) < glm::distance(eye, p2Pos))
		{	// p1 is closer than p2, but 
			// we are drawing from back to front, so switch them
			g_vecGameObjects[index] = p2;
			g_vecGameObjects[index + 1] = p1;
		}

	}//for ( int index = 0
}




// Moved to GLFW_keyboardCallback.cpp
//static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)

// HACK
cGameObject* pTP0 = 0;

int main(void)
{
	//	cAABBv2 testAABB(glm::vec3(-10.0f, -50.0f, -190.0f), 5.0f /*HALF size*/);

	srand(time(NULL));
	GLFWwindow* window;
	//    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	

	//// Other uniforms:
	// Moved to "global" scope 
	// (you might want to place these inside the shader class)
	//GLint uniLoc_materialDiffuse = -1;	
	//GLint uniLoc_materialAmbient = -1;   
	//GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
	//GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
	//							// w = shininess of the 
	//GLint uniLoc_eyePosition = -1;	// Camera position
	//GLint uniLoc_mModel = -1;
	//GLint uniLoc_mView = -1;
	//GLint uniLoc_mProjection = -1;


	if (!glfwInit())
	{
		// exit(EXIT_FAILURE);
		std::cout << "ERROR: Couldn't init GLFW, so we're pretty much stuck; do you have OpenGL??" << std::endl;
		return -1;
	}


	// Print to the console...(if a console is there)
	//std::cout << "Hello" << std::endl;
	//int q = 8;
	//std::cout << "Type a number:";
	//std::cin >> q;
	//std::cout << "You typed " << q << ". Hazzah." << std::endl;

	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";

	std::ifstream infoFile("config.txt");
	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		std::string a;

		infoFile >> a;	// "Game"	//std::cin >> a;
		infoFile >> a;	// "Config"
		infoFile >> a;	// "width"

		infoFile >> width;	// 1080

		infoFile >> a;	// "height"

		infoFile >> height;	// 768

		infoFile >> a;		// Title_Start

		std::stringstream ssTitle;		// Inside "sstream"
		bool bKeepReading = true;
		do
		{
			infoFile >> a;		// Title_End??
			if (a != "Title_End")
			{
				ssTitle << a << " ";
			}
			else
			{	// it IS the end! 
				bKeepReading = false;
				title = ssTitle.str();
			}
		} while (bKeepReading);


	}//if ( ! infoFile.is_open() )




	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// C++ string
	// C no strings. Sorry. char    char name[7] = "Michael\0";
	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// General error string, used for a number of items during start up
	std::string error;

	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	::g_pDebugRenderer = new cDebugRenderer();
	if (!::g_pDebugRenderer->initialize(error))
	{
		std::cout << "Warning: couldn't init the debug renderer." << std::endl;
	}

	// Load models
	::g_pModelAssetLoader = new cModelAssetLoader();
	::g_pModelAssetLoader->setBasePath("assets/models/");


	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, ::g_pModelAssetLoader, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}
	//LoadModelsIntoScene();
	::g_pLightManager = new cLightManager();
	::g_pTheCamera = new cCamera();
	::g_pTheCamera->eye = glm::vec3(0.0f, 0.0f, 60.0f);
	::g_pTheCamera->cameraMode = ::cCamera::eMode::FLY_CAMERA_USING_LOOK_AT;
	::g_pTheCamera->target = glm::vec3(0, 0, 0);
	::g_pTheCamera->theObject = new cGameObject();


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
	::g_pTextureManager = new CTextureManager();


	::g_pTextureManager->setBasePath("assets/textures");


	LoadModelsLightsFromFile();

	::g_pTextureManager->setBasePath("assets/textures/skybox");
	if (!::g_pTextureManager->CreateCubeTextureFromBMPFiles(
		"space",
		"SpaceBox_right1_posX.bmp",
		"SpaceBox_left2_negX.bmp",
		"SpaceBox_top3_posY.bmp",
		"SpaceBox_bottom4_negY.bmp",
		"SpaceBox_front5_posZ.bmp",
		"SpaceBox_back6_negZ.bmp", true, true))
	{
		std::cout << "Didn't load skybox" << std::endl;
	}



	::g_pLightManager->LoadShaderUniformLocations(currentProgID);


	//Go through all the loaded objects and make visual bounding boxes for all of them
	unsigned int sizeOfVector = (unsigned int)::g_vecGameObjects.size();
	for (int index = 0; index != sizeOfVector; index++)
	{
		cGameObject* pTheGO = ::g_vecGameObjects[index];

		

		cMesh mesh;
		::g_pVAOManager->lookupMeshFromName(pTheGO->meshName, mesh);
		
		//glm::vec3 maxExtent = mesh.maxExtentXYZ;
		//update its position based on the position of the ship
		//glm::vec4 tempPosMin = glm::toMat4(pTheGO->qOrientation) * (glm::vec4(mesh.minXYZ,1) *pTheGO->scale);
		//glm::vec4 tempPosMax = glm::toMat4(pTheGO->qOrientation) * (glm::vec4(mesh.maxXYZ, 1)*pTheGO->scale);

		glm::vec4 tempPosMin = (glm::toMat4(pTheGO->qOrientation) * glm::vec4(mesh.minXYZ, 1))*pTheGO->scale;
		glm::vec4 tempPosMax = (glm::toMat4(pTheGO->qOrientation) * glm::vec4(mesh.maxXYZ, 1))*pTheGO->scale;

		glm::vec3 min = glm::vec3(tempPosMin.x + pTheGO->position.x
			, tempPosMin.y + pTheGO->position.y,
			tempPosMin.z + pTheGO->position.z);
		glm::vec3 max = glm::vec3(tempPosMax.x + pTheGO->position.x
			, tempPosMax.y + pTheGO->position.y,
			tempPosMax.z + pTheGO->position.z);

		//make a box of triangles
		//bottom
		::g_pDebugRenderer->addTriangle(min,
			glm::vec3(max.x, min.y, min.z),
			glm::vec3(min.x, min.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);
		::g_pDebugRenderer->addTriangle(glm::vec3(max.x, min.y, min.z),
			glm::vec3(max.x, min.y, max.z),
			glm::vec3(min.x, min.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);

		//top
		::g_pDebugRenderer->addTriangle(glm::vec3(min.x, max.y, min.z),
			glm::vec3(max.x, max.y, min.z),
			glm::vec3(min.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);
		::g_pDebugRenderer->addTriangle(glm::vec3(max.x, max.y, min.z),
			glm::vec3(max.x, max.y, max.z),
			glm::vec3(min.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);

		//left
		::g_pDebugRenderer->addTriangle(min,
			glm::vec3(min.x, min.y, max.z),
			glm::vec3(min.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);
		::g_pDebugRenderer->addTriangle(min,
			glm::vec3(min.x, max.y, min.z),
			glm::vec3(min.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);

		//right
		::g_pDebugRenderer->addTriangle(glm::vec3(max.x, min.y, min.z),
			glm::vec3(max.x, min.y, max.z),
			glm::vec3(max.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);
		::g_pDebugRenderer->addTriangle(glm::vec3(max.x, max.y, max.z),
			glm::vec3(max.x, max.y, min.z),
			glm::vec3(max.x, min.y, min.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);

		//front
		::g_pDebugRenderer->addTriangle(glm::vec3(max.x, min.y, min.z),
			glm::vec3(max.x, min.y, max.z),
			glm::vec3(max.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);
		::g_pDebugRenderer->addTriangle(glm::vec3(max.x, min.y, min.z),
			glm::vec3(max.x, max.y, min.z),
			glm::vec3(max.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);

		//back
		::g_pDebugRenderer->addTriangle(glm::vec3(min.x, min.y, min.z),
			glm::vec3(min.x, min.y, max.z),
			glm::vec3(min.x, max.y, max.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);
		::g_pDebugRenderer->addTriangle(glm::vec3(min.x, max.y, max.z),
			glm::vec3(min.x, max.y, min.z),
			glm::vec3(min.x, min.y, min.z),
			glm::vec3(1.0f, 0.0f, 0.0f), 1000);

	}//for ( int index = 0...

	::g_pTestParticles = new cEmitter(glm::vec3(0.0, -9.9, 0));
	//::g_pTestParticles->position = glm::vec3(0.99, -19.9, 1.23);

	::g_pTestParticles->init(20, 20,
		glm::vec3(0.1f, 0.3f, 0.1f),	// Min init vel
		glm::vec3(0.2f, 0.9f, 0.2f),	// max init vel
		2.0f, 6.0f,
		glm::vec3(-1.0f, -1.0f, -1.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),
		glm::vec3(0.0f, 0.0f, 0.0f));



	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();



	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		sortObjectsBasedOnCamera();
		// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		::g_pTheCamera->updateTick(deltaTime);

		RenderScene(::g_vecGameObjects, window, deltaTime);

		// "Presents" what we've drawn
		// Done once per scene 
		glfwSwapBuffers(window);
		glfwPollEvents();


		LightFlicker(curTime);


		::g_pTestParticles->Update((float)deltaTime);

		lastTimeStep = curTime;

	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;

	//    exit(EXIT_SUCCESS);
	return 0;
}

void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID)
{
	switch (samplerIndex)
	{
	case 0:
		glUniform1i(texSampCube00_LocID, textureUnitID);
		glUniform1f(texCubeBlend00_LocID, blendRatio);
		break;
	case 1:
		glUniform1i(texSampCube01_LocID, textureUnitID);
		glUniform1f(texCubeBlend01_LocID, blendRatio);
		break;
	case 2:
		glUniform1i(texSampCube02_LocID, textureUnitID);
		glUniform1f(texCubeBlend02_LocID, blendRatio);
		break;
	case 3:
		glUniform1i(texSampCube03_LocID, textureUnitID);
		glUniform1f(texCubeBlend03_LocID, blendRatio);
		break;
	default:
		// Invalid samplerIndex;
		break;
	}//switch (samplerIndex)
	return;
}//void setCubeSamplerAndBlenderByIndex()

void LightFlicker(double curTime)
{

	curTime *= 10;
	int randNum = rand() % 2;
	if ((int)curTime % 6 > 3 && ::g_pLightManager->vecLights.size() > 1)
	{
		if (randNum == 1)
		{
			::g_pLightManager->vecLights.back().diffuse = glm::vec3(0.0f, 0.0f, 0.0f);
		}
		else
		{
			::g_pLightManager->vecLights[::g_pLightManager->vecLights.size() - 2].diffuse =
				glm::vec3(0.0f, 0.0f, 0.0f);
		}
	}
	else
	{
		if (randNum == 1)
		{
			::g_pLightManager->vecLights.back().diffuse = glm::vec3(1.0f, 0.1f, 0.1f);
		}
		else
		{
			::g_pLightManager->vecLights[::g_pLightManager->vecLights.size() - 2].diffuse =
				glm::vec3(1.0f, 0.1f, 0.1f);
		}
	}
}
