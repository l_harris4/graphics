#include "cCamera.h"
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective


cCamera::cCamera()
{
	// Default OpenGL 1.1 was on z axis looking at the origin
	this->eye = glm::vec3(0.0f, 0.0f, 1.0f);
	this->target = glm::vec3(0.0f, 0.0f, 0.0f);
	this->up = glm::vec3(0.0f, 1.0f, 0.0f);

	// Set quaternion to some default rotation value
	this->qOrientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

	this->accel = glm::vec3(0.0f);
	this->velocity = glm::vec3(0.0f);

	return;
}


// For following, etc. 
void cCamera::updateTick(double deltaTime)
{


	return;
}

void cCamera::setCameraMode(eMode cameraMode)
{
	// Yes, it's an enum, but you can pass anything, so double-check
	switch (cameraMode)
	{
	case cCamera::eMode::FLY_CAMERA_GARBAGE_DONT_USE:
		cameraMode = cCamera::FLY_CAMERA_USING_LOOK_AT;
		assert("What are you doing with your life? DON'T use the garbage camera");
		break;
	case cCamera::eMode::FLY_CAMERA_USING_LOOK_AT:
	case cCamera::eMode::FOLLOW_CAMERA:
	case cCamera::eMode::MANUAL:
		this->cameraMode = cameraMode;
		return;
	}

	// Something is wrong, so set to manual
	this->cameraMode = cCamera::eMode::MANUAL;
	return;
}
glm::mat4 cCamera::getViewMatrix(void)
{
	// Based on the mode, calculate the view matrix
	switch (cameraMode)
	{
	case cCamera::eMode::MANUAL:
		glm::mat4 matCamView = glm::mat4(1.0f);

		glm::mat4 trans = glm::mat4x4(1.0f);
		trans = glm::translate(trans, eye);
		matCamView = matCamView * trans;

		// Like many things in GML, the conversion is done in the constructor
		glm::mat4 postRotQuat = glm::mat4(this->qOrientation);
		matCamView = matCamView * postRotQuat;

		return matCamView;
		break;
	case cCamera::eMode::FOLLOW_CAMERA:
	{
		glm::mat4 matView = glm::lookAt(this->eye,
			this->target,
			glm::vec3(0.0f, 1.0f, 0.0f)); // UP
		this->qOrientation = glm::quat(matView);
		return matView;
	}
	break;
	case cCamera::eMode::FLY_CAMERA_USING_LOOK_AT:
	{
		glm::mat4 matView = glm::lookAt(this->eye,
			this->target,
			glm::vec3(0.0f, 1.0f, 0.0f)); // UP
		this->qOrientation = glm::quat(matView);
		return matView;
	}
	break;

	// ************************************************************************
	// This quaternion based mess is, well, a mess. Don't use as is
	case cCamera::eMode::FLY_CAMERA_GARBAGE_DONT_USE:
		// Use same process as with drawing an object:
		// Combine transform with rotation, and return that

		glm::mat4 matCamTrans = glm::mat4x4(1.0f);
		matCamTrans = glm::translate(matCamTrans, this->eye);
		//		matCamView = matCamView * matCamTrans;

		// Like many things in GML, the conversion is done in the constructor
		glm::mat4 matCamRotate = glm::mat4(this->qOrientation);

		glm::mat4 matCamViewT = matCamTrans * matCamRotate;

		return matCamViewT;
		break;
		// ************************************************************************
	}
	// You need to check what you are doign with your life!
	// return the identity matrix
	return glm::mat4(1.0f);
}



//// ************************************************************
//
//void cCamera::setOrientationFromEuler(glm::vec3 eulerAngles);
//glm::mat4 cCamera::getMat4FromOrientation(void);

void cCamera::overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation)
{
	// Calcualte the quaternion represnetaiton of this Euler axis
	// NOTE: We are OVERWRITING this..
	this->qOrientation = glm::quat(eulerAxisOrientation);

	return;
}

void cCamera::adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange)
{
	// How do we combine two matrices?
	// That's also how we combine quaternions...

	// So we want to "add" this change in oriention
	glm::quat qRotationChange = glm::quat(eulerAxisOrientChange);

	// Mulitply it by the current orientation;
	this->qOrientation = this->qOrientation * qRotationChange;

	return;
}
