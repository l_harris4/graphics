#ifndef _cCamera_HG_
#define _cCamera_HG_

#include <glm/vec3.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include "cGameObject.h"

class cCamera
{
public:
	cCamera();

	glm::vec3 eye;			// position
	glm::vec3 target;
	glm::vec3 up;

	glm::vec3 velocity;		// For "fly camera", like in blender
	glm::vec3 accel;		// For "thruster" like with rockets

							// For following, etc. 
	void updateTick(double deltaTime);
	cGameObject* theObject;

	enum eMode
	{
		MANUAL,			// Move along the axes (lookat)
		FOLLOW_CAMERA,	// Follows a target (lookat)
		FLY_CAMERA_USING_LOOK_AT,	// Here, you use the "target" as direction
									// you want to go. This allows you to transition
									// from the FOLLOW_CAMERA to FLY seamlessly


									FLY_CAMERA_GARBAGE_DONT_USE		// Movement based on direction of gaze
																	// Use quaternion orientation
																	// "catch"  is no LOOKAT
	};

	void setCameraMode(eMode cameraMode);
	eMode cameraMode;


	glm::vec3 follow_idealCameraLocationRelToTarget;
	float follow_max_speed;
	float follow_distance_max_speed;
	float follow_distance_zero_speed;
public:
	// ************************************************************

	void overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation);
	// NOTE: Use THIS, not just setting the values
	void adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange);

	//	glm::mat4 getMat4FromOrientation(void);
	glm::mat4 getViewMatrix(void);


	// 
	glm::quat qOrientation;

	glm::vec3 EulerAngles;	// Ya get gimbal lock, yo.
};

#endif
